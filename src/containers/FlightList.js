import React, { Component } from "react";
import { connect } from "react-redux";
import Select from 'react-select';
import _ from "lodash";

import { selectCarrier } from "../data/actions";

import FlightCard from "../components/FlightCard";

class FlightList extends Component {

  handleChange = carrier => {
    this.props.onSelectCarrier(carrier.value);
  }

  render(){
    const options = this.props.carriers.map(carrier => ({
      value: carrier,
      label: carrier,
    }));
    const flights = this.props.visibleFlights;

    return(<div>
      <Select
        name="form-field-name"
        placeholder={"Select carrier..."}
        value={this.props.selectedCarrier}
        onChange={this.handleChange}
        options={options}
        clearable={false}
      />
      {
        flights.map((flight, idx) => <FlightCard key={idx} flight={flight}/>)
      }
    </div>);
  }
}

const mapStateToProps = (state) => ({
  visibleFlights: state.visibleFlights,
  selectedCarrier: state.selectedCarrier,
  carriers: _.uniq(state.flights.map(flight => (flight.carrier))),
});

const mapDispatchToProps = {
  onSelectCarrier: selectCarrier,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlightList);