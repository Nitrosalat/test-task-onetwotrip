import { createAction } from "redux-actions";

export const selectCarrier = createAction("SELECT_CARRIER", carrier => carrier);