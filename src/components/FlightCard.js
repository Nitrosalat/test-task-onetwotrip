import React, { Component } from "react";
import "./FlightCard.css";
import moment from "moment";

export default class FlightCard extends Component {
  render() {
    const {
      direction: { from, to },
      carrier,
      departure,
      arrival,
    } = this.props.flight;

    const departureFormatted = moment(departure).format("DD.MM hh:mm");
    const arrivalFormatted = moment(arrival).format("DD.MM hh:mm");

    return (
      <div className="flight-card">
        <p>{from} - {to}</p>
        <p>Departure: {departureFormatted}</p>
        <p>Arrival : {arrivalFormatted}</p>
        <p>{carrier}</p>
      </div>
    );
  }
}