import 'react-select/dist/react-select.css';

import React, { Component } from 'react';
import './App.css';

import FlightList from "./containers/FlightList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <FlightList/>
      </div>
    );
  }
}

export default App;
